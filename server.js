const validator = require('validator');
const express = require('express');
const rp = require('request-promise');
const events = require('events');

const app = express()
const eventEmitter = new events.EventEmitter();

const PORT = 3000
const RESULTS_EVENT_NAME = 'results'
const MAX_REQUESTS = TOTAL_JOBS = 100
const API_URL = 'https://anapioficeandfire.com/api/characters'
const SECOND = 1000
const BATCH_SIZE = 10

class ConcurrentRequestsHandler {
  constructor() {
    this.requests = []
    this.done = []
    this.failed = []
    this.total_batches = 0

    this.generateRequestUrls()
    this.validate()
  }

  generateRequestUrls() {
    if(!this.requests.length) {
      for(let i=0; i<MAX_REQUESTS; i++) {
        this.requests.push(`${API_URL}/${i}`)
      }
    }
  }

  validate() {
    if(this.requests.length !== MAX_REQUESTS) {
      throw new Error('Expected 100 items for `requests` variable')
    }
  }

  runBatch() {
    let urls = this.requests.splice(0, BATCH_SIZE)
    urls.forEach(url => {
      setTimeout(()=>{
        rp(url)
          .then(htmlString => this.done.push(htmlString))
          .catch(err => this.failed.push({url, err}))
          .then(()=>{
            if(requestsHandler.done.length + requestsHandler.failed.length
                  == TOTAL_JOBS) {
              eventEmitter.emit(RESULTS_EVENT_NAME)
            }
          })
      }, 1)
    })
  }

  run() {
    let runBatchCron = setInterval(()=>{
      if(this.total_batches < BATCH_SIZE) {
        this.total_batches++
        return this.runBatch()
      }

      clearInterval(runBatchCron)
    }, SECOND)

  }
}

app.get('/', (req, res) => {
  eventEmitter.addListener(RESULTS_EVENT_NAME, ()=>{
    return res.send({
      "done": requestsHandler.done,
      "failed": requestsHandler.failed
    })
  });

  requestsHandler = new ConcurrentRequestsHandler()
  requestsHandler.run()
})

app.listen(PORT, () => console.log(`Server listening on port ${PORT}!`))
